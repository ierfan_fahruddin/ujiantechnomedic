<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    function register()
    {
        return view('auth.register');
    }
    function store(Request $request)
    {
        $validatedData = $request->validate([
            'username' => 'required|min:3|max:20|unique:users',
            'fullname' => 'required|unique:users',
            'password' => 'required|min:3'
        ]);
        $validatedData['is_active'] = 0;
        $validatedData['role'] = 'user';
        $validatedData['password'] = Hash::make($validatedData['password']);
        User::insert($validatedData);
        return redirect('/login')->with('success', 'berhasil register');
    }
    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $user = User::where('username', $request->username)->where('is_active', 0)->exists();
        if ($user) {
            return redirect('/login')->with('error', 'Akun anda belum aktif');
        }
        $credentials = $request->validate([
            'username' => 'required',
            'password' => 'required|max:255'
        ]);
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('/');
        }
        return back()->with('LoginError', 'Login Failed!, Silahkan Coba Lagi');
    }

    public function logout()
    {

        Auth::logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        return redirect('/login');
    }
}