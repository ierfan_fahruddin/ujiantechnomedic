<?php

namespace App\Http\Controllers;

use App\Models\JenisObat;
use App\Models\Obat;
use Illuminate\Http\Request;

class ObatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['obat'] = Obat::with('jenisobat')->orderBy('id', 'desc')->paginate(20);
        return view('pages.obat.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['jenisobat'] = JenisObat::all();
        return view('pages.obat.tambah', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $obat = new Obat();
        $obat->nama_obat = $request->nama_obat;
        $obat->id_jenis_obat = $request->id_jenis_obat;
        $obat->satuan = $request->satuan;
        $obat->harga = $request->harga;
        $obat->stok = $request->stok;
        $obat->tgl_exp = $request->tgl_exp;
        $obat->save();
        return redirect('obat')->with('success', 'berhasil menambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JenisObat  $jenisObat
     * @return \Illuminate\Http\Response
     */
    public function show(JenisObat $jenisObat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JenisObat  $jenisObat
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['jenisobat'] = JenisObat::all();
        $data['obat'] = Obat::find($id);
        return view('pages.obat.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JenisObat  $jenisObat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $obat = Obat::find($request->id);
        $obat->nama_obat = $request->nama_obat;
        $obat->id_jenis_obat = $request->id_jenis_obat;
        $obat->satuan = $request->satuan;
        $obat->harga = $request->harga;
        $obat->stok = $request->stok;
        $obat->tgl_exp = $request->tgl_exp;
        $obat->update();
        return redirect('obat')->with('success', 'berhasil mengedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JenisObat  $jenisObat
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $obat = Obat::find($id);
        $obat->delete();
        return redirect('obat')->with('success', 'berhasil menghapus');
    }
}