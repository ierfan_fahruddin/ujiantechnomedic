<?php

namespace App\Http\Controllers;

use App\Models\JenisObat;
use App\Models\Obat;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    function index()
    {

        $data['obat1'] = Obat::with('jenisobat')->orderBy('tgl_exp', 'asc')->paginate(15);
        $data['obat'] = Obat::all()->count();
        $data['obatsudahexp'] = Obat::where('tgl_exp', '<=', now())->count();
        $data['obatbelumexp'] = Obat::where('tgl_exp', '>=', now())->count();
        $data['jenisobat'] = JenisObat::all()->count();
        $data['user'] = User::all()->count();
        $data['user_active'] = User::where('is_active', 1)->get()->count();
        $data['user_nonactive'] = User::where('is_active', 0)->get()->count();
        return view('index', $data);
    }
}