<?php

namespace App\Http\Controllers;

use App\Models\JenisObat;
use Illuminate\Http\Request;

class JenisObatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['jenisobat'] = JenisObat::orderBy('id', 'desc')->paginate(20);
        return view('pages.jenis_obat.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.jenis_obat.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jenisonbat = new JenisObat();
        $jenisonbat->nama_jenis_obat = $request->nama_jenis_obat;
        $jenisonbat->save();
        return redirect('/jenis_obat')->with('success', 'berhasil menambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JenisObat  $jenisObat
     * @return \Illuminate\Http\Response
     */
    public function show(JenisObat $jenisObat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JenisObat  $jenisObat
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['jenisobat'] = JenisObat::find($id);
        return view('pages.jenis_obat.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JenisObat  $jenisObat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $jenisobat = JenisObat::find($request->id);
        $jenisobat->nama_jenis_obat = $request->nama_jenis_obat;
        $jenisobat->update();
        return redirect('/jenis_obat')->with('success', 'berhasil mengedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JenisObat  $jenisObat
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jenisobat = JenisObat::find($id);
        $jenisobat->delete();
        return redirect('/jenis_obat')->with('success', 'berhasil menghapus');
    }
}