<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\JenisObatController;
use App\Http\Controllers\ObatController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/jenis_obat', function () {
//     return view('pages/jenis_obat/index');
// });

Route::get('/login', [AuthController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'store']);

Route::get('/register', [AuthController::class, 'register']);
Route::get('/logout', [AuthController::class, 'logout']);
Route::group([
    'middleware' => 'auth',
], function () {

    Route::get('/', [DashboardController::class, 'index'])->name('home');
    Route::get('/jenis_obat', [JenisObatController::class, 'index']);
    Route::get('/jenis_obat/create', [JenisObatController::class, 'create']);
    Route::get('/jenis_obat/edit/{id}', [JenisObatController::class, 'edit']);
    Route::get('/jenis_obat/delete/{id}', [JenisObatController::class, 'destroy']);
    Route::post('/jenis_obat/store', [JenisObatController::class, 'store']);
    Route::post('/jenis_obat/update', [JenisObatController::class, 'update']);


    Route::get('/obat', [ObatController::class, 'index']);
    Route::get('/obat/create', [ObatController::class, 'create']);
    Route::get('/obat/edit/{id}', [ObatController::class, 'edit']);
    Route::get('/obat/delete/{id}', [ObatController::class, 'destroy']);
    Route::post('/obat/store', [ObatController::class, 'store']);
    Route::post('/obat/update', [ObatController::class, 'update']);


    Route::get('/user', [UserController::class, 'index'])->middleware('role:superadmin');
    Route::get('/user/useractive/{id}', [UserController::class, 'useractive'])->middleware('role:superadmin');
    Route::get('/user/usernonactive/{id}', [UserController::class, 'usernonactive'])->middleware('role:superadmin');
    Route::get('/user/create', [UserController::class, 'create'])->middleware('role:superadmin');
    Route::get('/user/edit/{id}', [UserController::class, 'edit'])->middleware('role:superadmin');
    Route::get('/user/delete/{id}', [UserController::class, 'destroy'])->middleware('role:superadmin');
    Route::post('/user/store', [UserController::class, 'store'])->middleware('role:superadmin');
    Route::post('/user/update', [UserController::class, 'update'])->middleware('role:superadmin');
});