<form action="{{url('/jenis_obat/update')}}" method="post">
    {{csrf_field()}}
    <div class="p2">
        <div class="form-group mb-3">
            <input type="hidden" name="id" value="{{ $jenisobat->id }}">
            <label class="form-label" for="nama_jenis_obat">Nama Jenis Obat</label>
            <input class="form-control" value="{{$jenisobat->nama_jenis_obat}}" required type="text" name="nama_jenis_obat" id="nama_jenis_obat">
        </div>
        <button class="btn btn-primary" type="submit">Submit</button>
    </div>
</form>
