<form action="{{url('/obat/store')}}" method="post">
    {{csrf_field()}}
    <div class="p2">
        <div class="form-group mb-3">
            <label class="form-label" for="nama_obat">Nama Obat</label>
            <input class="form-control" required type="text" name="nama_obat" id="nama_obat">
        </div>
        
        <div class="form-group mb-3">
            <label class="form-label" for="id_jenis_obat"> Jenis Obat</label>
            <select required class="form-control" name="id_jenis_obat" id="id_jenis_obat">
                <option value="">---</option>
                @foreach ($jenisobat as $row)
                    <option value="{{ $row->id }}">{{ $row->nama_jenis_obat }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group mb-3">
            <label class="form-label" for="satuan">Satuan</label>
            <input class="form-control" required type="text" name="satuan" id="satuan">
        </div>
        <div class="form-group mb-3">
            <label class="form-label" for="harga">Harga</label>
            <input class="form-control" required type="number" name="harga" id="harga">
        </div>
        <div class="form-group mb-3">
            <label class="form-label" for="stok">Stok</label>
            <input class="form-control" required type="number" name="stok" id="stok">
        </div>
        <div class="form-group mb-3">
            <label class="form-label" for="tgl_exp">Tgl Exp</label>
            <input class="form-control" required type="date" name="tgl_exp" id="tgl_exp">
        </div>
        <button class="btn btn-primary" type="submit">Submit</button>
    </div>
</form>
