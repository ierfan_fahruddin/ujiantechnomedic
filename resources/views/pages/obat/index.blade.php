@extends('layout.main')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Obat</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Obat</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
              @endif @if(session('success'))
              <div class="d-flex justify-content-center w-100 mt-3">
                  <div class="alert alert-success alert-dismissible fade show w-75" role="alert">
                      <p class="text-center fs-6">{{session('success')}}</p>
                  </div>
              </div>
          @endif
              <div class="card-header">
                <a href="#" onclick="create()" class="btn btn-success">Tambah</a>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-striped table-hover">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Obat</th>
                    <th> Jenis Obat</th>
                    <th>Satuan</th>
                    <th>Harga</th>
                    <th>Stok</th>
                    <th>Total Harga Stok</th>
                    <th>Tgl Exp</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach ($obat as $row)
                        <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$row->nama_obat}}</td>
                    <td>{{$row->jenisobat->nama_jenis_obat}}</td>
                    <td>{{$row->satuan}}</td>
                    <td>{{$row->harga}}</td>
                    <td>{{$row->stok}}</td>
                    <td>Rp.{{ number_format($row->harga * $row->stok, 0, ',', '.') }}</td>
                    <td> @if($row->tgl_exp <= now())
                      <span class="text-danger">

                          {{$row->tgl_exp}}
                      </span>
                      @else
                      
                      {{$row->tgl_exp}}
                      @endif</td>
                    <td>
                      <button onclick="edit('{{$row->id}}')" class="btn btn-primary"><i class="fas fa-pen"></i></button>
                      <a href="{{url('/obat/delete')}}/{{$row->id}}" onclick="return confirm('Apa Anda Yakin??')" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                    </td>
                  </tr>
                    @endforeach
                  
                  </tfoot>
                </table>
              </div>
              {{$obat->links()}}
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <div class="modal fade" id="obat" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="judul"></h5>
                  <i class="bi bi-x-lg" style="font-size: 20px;" data-dismiss="modal" aria-label="Close"></i>
                  <!-- <button type="button" class="btn-close"></button> -->
              </div>
              <div class="modal-body">
                  <div id="page"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
  </div>
  
@endsection
@push('js')
<script>
  function create() {
        $.get("{{url('/obat/create')}}", {}, function(data, status) {
            $('#judul').html('Tambah Obat');
            $('#page').html(data);
            $('#obat').modal('show');
        });
    }
    function edit(id) {
        $.get("{{url('/obat/edit')}}/" + id, {}, function(data, status) {
            $('#judul').html('Edit Obat');
            $('#page').html(data);
            $('#obat').modal('show');
        });
    }
</script>
@endpush